��    E      D  a   l      �     �     �       	     Q     =   n     �  5   �  $   �  	        %     .     6     H     g     y  \   �     �     �     �     �     
          "  >   :  :   y     �  	   �  
   �  	   �  
   �     �     �     	     	     /	     F	     U	     d	     q	  
   w	     �	  +   �	  %   �	     �	     �	     �	  
   
  @   
  5   X
  a   �
  d   �
      U  6   v  6   �  $   �  1   	  Q   ;     �     �     �  	   �  
   �     �     �  H   �     2     8  q  =     �     �     �  	   �  Q   �  =   ,     j  �   ~  $   O  	   t     ~     �     �     �     �     �  \   �     @     E     L     T     c     l     {  >   �  :   �       	     
   $  	   /  
   9     D     R     `     o     �     �     �     �     �  
   �     �  @   �  3   2     f     s     z  
   �  @   �  5   �  a   
  d   l  e   �  6   7  6   n  $   �  1   �  Q   �     N     Z     p  	   v  
   �     �     �  H   �     �     �         C          &       9   0                         7   D   !         $           	                B         E   #       <              2      "                                ;                          6   =   '   >       ?   5       @         *       4      
   .   /   ,                A       +   )   %   1   8           (          3       -      :    Access Code Add New Add New Room All Rooms Bigbluebutton depends on the font awesome plugin. Please install and activate it. Bigbluebutton depends on the heartbeat API. Please enable it. Collapse recordings Default server settings 1. Default server settings 2. Displays a BigBlueButton login form. Edit Room EndPoint Example Expand recordings Failed to import the room, %s. Filter rooms list Insert into room It is currently not possible to create rooms on the server. Please contact support for help. Join Manage Meeting Moderator Code New Room No rooms found No rooms found in trash Not all room logs were able to be imported to the new version. Not all rooms were able to be imported to the new version. Presentation Protected Recordable Recording Recordings Room Archives Room Settings Room published Room published privately Room reverted to draft Room scheduled Room selection Room updated Rooms Rooms list Rooms list navigation Save server settings bad url error message. Save server settings success message. Search Rooms Server Shared Secret Statistics The access code you have entered is incorrect. Please try again. The form has expired or is invalid. Please try again. The meeting has not started yet. Please wait for a moderator to start the meeting before joining. The meeting has not started yet. You will be automatically redirected to the meeting when it starts. The server settings explanation. The token: %s is not associated with a published room. The token: %s is not associated with an existing room. There are no rooms in the selection. This room does not currently have any recordings. This user does not have permission to display any rooms in a shortcode or widget. Unprotected Uploaded to this room Video View Room View Rooms Viewer Code Wait for Moderator You do not have permission to enter the room. Please request permission. false true Project-Id-Version: 
PO-Revision-Date: 2019-10-16 10:43-0400
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
Last-Translator: 
Language: en_CA
X-Poedit-SearchPath-0: admin/partials
X-Poedit-SearchPath-1: public/partials
 Access Code Add New Add New Room All Rooms Bigbluebutton depends on the font awesome plugin. Please install and activate it. Bigbluebutton depends on the heartbeat API. Please enable it. Collapse recordings Note that the values included by default are using a FREE BigBlueButton server provided by Blindside Networks. They have to be replaced with the parameters obtained from a server better suited for production. Displays a BigBlueButton login form. Edit Room EndPoint Example Expand recordings Failed to import the room, %s. Filter rooms list Insert into room It is currently not possible to create rooms on the server. Please contact support for help. Join Manage Meeting Moderator Code New Room No rooms found No rooms found in trash Not all room logs were able to be imported to the new version. Not all rooms were able to be imported to the new version. Presentation Protected Recordable Recording Recordings Room Archives Room Settings Room published Room published privately Room reverted to draft Room scheduled Room Selection Room updated Rooms Rooms list Rooms list navigation Error: the URL you have entered must end with '/bigbluebutton/'. Success! Your room server settings have been saved. Search Rooms Server Shared Secret Statistics The access code you have entered is incorrect. Please try again. The form has expired or is invalid. Please try again. The meeting has not started yet. Please wait for a moderator to start the meeting before joining. The meeting has not started yet. You will be automatically redirected to the meeting when it starts. The settings listed below determine the BigBlueButton server that will be used for the live sessions. The token: %s is not associated with a published room. The token: %s is not associated with an existing room. There are no rooms in the selection. This room does not currently have any recordings. This user does not have permission to display any rooms in a shortcode or widget. Unprotected Uploaded to this room Video View Room View Rooms Viewer Code Wait for Moderator You do not have permission to enter the room. Please request permission. false true 